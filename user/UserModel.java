package com.srpmed.user;

import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.srpmed.medical_registration.MedicalRegistrationModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * User's data object in database
 */
@Entity(name = "User")
@Table(name = "users")
public class UserModel implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private long userId;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(name = "birth_date", nullable = false)
    private Date birthDate;

    @Column(nullable = false)
    private String pesel;

    @Column(nullable = false)
    private String sex;

    @Column
    private String token;

    @Column(columnDefinition = "boolean default false")
    private boolean active;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private UserAddressModel addressModel;

    @OneToMany(mappedBy = "owner")
    private List<UserRequest> userRequests;

    @OneToMany(mappedBy = "owner")
    private List<MedicalRegistrationModel> registrations;

    @Column
    private String roles;

    // user details methods
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final Iterable<String> roles = Splitter.on(',').split(this.roles);
        final List<GrantedAuthority> grantedAuthorities = new LinkedList<>();

        final Iterator<String> iterator = roles.iterator();

        while(iterator.hasNext()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(iterator.next()));
        }

        return grantedAuthorities;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate == null ? null : new Date(birthDate.getTime());
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate == null ? null : new Date(birthDate.getTime());
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public UserAddressModel getAddressModel() {
        return addressModel;
    }

    public void setAddressModel(UserAddressModel addressModel) {
        this.addressModel = addressModel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<UserRequest> getUserRequests() {
        return userRequests;
    }

    public void setUserRequests(List<UserRequest> userRequests) {
        this.userRequests = userRequests;
    }

    public List<MedicalRegistrationModel> getRegistrations() {
        return this.registrations;
    }

    public void setRegistrations(List<MedicalRegistrationModel> registrations) {
        this.registrations = registrations;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserModel)) return false;
        UserModel userModel = (UserModel) o;
        return userId == userModel.userId &&
                active == userModel.active &&
                Objects.equal(firstName, userModel.firstName) &&
                Objects.equal(lastName, userModel.lastName) &&
                Objects.equal(email, userModel.email) &&
                Objects.equal(password, userModel.password) &&
                Objects.equal(birthDate, userModel.birthDate) &&
                Objects.equal(pesel, userModel.pesel) &&
                Objects.equal(sex, userModel.sex) &&
                Objects.equal(token, userModel.token) &&
                Objects.equal(addressModel, userModel.addressModel) &&
                Objects.equal(userRequests, userModel.userRequests) &&
                Objects.equal(registrations, userModel.registrations) &&
                Objects.equal(roles, userModel.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(userId, firstName, lastName, email, password, birthDate, pesel, sex, token, active, addressModel, userRequests, registrations, roles);
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", birthDate=" + birthDate +
                ", pesel='" + pesel + '\'' +
                ", sex='" + sex + '\'' +
                ", token='" + token + '\'' +
                ", active=" + active +
                ", addressModel=" + addressModel +
                ", userRequests=" + userRequests +
                ", registrations=" + registrations +
                ", roles='" + roles + '\'' +
                '}';
    }
}