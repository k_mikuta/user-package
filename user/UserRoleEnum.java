package com.srpmed.user;

/**
 * Enum defines User Roles
 */
public enum UserRoleEnum {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_USER("ROLE_USER");

    private final String roleName;

    UserRoleEnum(String roleName) {
        this.roleName = roleName;
    }

    public String roleName() {
        return roleName;
    }
}
