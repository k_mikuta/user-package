package com.srpmed.user;

import com.google.common.base.Preconditions;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * User model service objective layer
 */
@Service
public class UserModelSLO {

    private final UserModelRepository userModelRepository;

    public UserModelSLO(UserModelRepository userModelRepository) {
        this.userModelRepository = userModelRepository;
    }

    public UserModel findUserByEmail(String email) {
        Preconditions.checkArgument(email != null);

        UserModel user = userModelRepository.findByEmail(email);
        if(user == null ) {
            throw new UsernameNotFoundException("User with email " + email + " does not exists");
        }
        return user;
    }

    public UserModel saveUser(UserModel userModel) {
        Preconditions.checkArgument(userModel != null);

        return userModelRepository.save(userModel);
    }

    public UserModel findByToken(String token) {
        Preconditions.checkArgument(token != null);

        UserModel user = userModelRepository.findByToken(token);
        if(user == null ) {
            throw new UsernameNotFoundException("User with token " + token + " does not exists");
        }
        return user;
    }

    public UserModel findActiveUserByEmail(String email) {
        Preconditions.checkArgument(email != null);

        UserModel user = userModelRepository.findByEmailAndActive(email, true);

        if(user == null ) {
            throw new UsernameNotFoundException("Active user with email " + email + " does not exists");
        }
        return user;
    }
}
