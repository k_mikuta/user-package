package com.srpmed.user;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User's adress data object in database
 */
@Entity
@Table(name = "user_addresses")
public class UserAddressModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id", nullable = false)
    private long addressId;

    @Column(name = "street_name", nullable = false)
    private String streetName;

    @Column(name = "house_number", nullable = false)
    private String houseNumber;

    @Column(name = "flat_number")
    private String flatNumber;

    @Column(nullable = false)
    private String postcode;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String province;

    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAddressModel)) return false;
        UserAddressModel that = (UserAddressModel) o;
        return addressId == that.addressId &&
                Objects.equal(streetName, that.streetName) &&
                Objects.equal(houseNumber, that.houseNumber) &&
                Objects.equal(flatNumber, that.flatNumber) &&
                Objects.equal(postcode, that.postcode) &&
                Objects.equal(city, that.city) &&
                Objects.equal(province, that.province);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(addressId, streetName, houseNumber, flatNumber, postcode, city, province);
    }

    @Override
    public String toString() {
        return "UserAddressModel{" +
                "addressId=" + addressId +
                ", streetName='" + streetName + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", flatNumber='" + flatNumber + '\'' +
                ", postcode='" + postcode + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                '}';
    }
}