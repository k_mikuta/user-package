package com.srpmed.user;

import javax.persistence.*;

/**
 * Model representing user's requests
 *
 * @author Mateusz Tarza
 */
@Entity
@Table(name = "user_requests")
public class UserRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_request_id", nullable = false)
    private long userRequestId;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel owner;

    @Column(nullable = false)
    private String token;

    public long getUserRequestId() {
        return userRequestId;
    }

    public void setUserRequestId(long userRequestId) {
        this.userRequestId = userRequestId;
    }

    public UserModel getOwner() {
        return owner;
    }

    public void setOwner(UserModel owner) {
        this.owner = owner;
    }
}
