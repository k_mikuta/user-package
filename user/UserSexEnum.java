package com.srpmed.user;

/**
 * User's sex types
 */
public enum UserSexEnum {

    MALE("MALE"),
    FEMALE("FEMALE");

    private String code;

    UserSexEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}