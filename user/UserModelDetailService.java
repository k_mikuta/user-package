package com.srpmed.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service provides authorization process
 */
@Service
public class UserModelDetailService implements UserDetailsService {

    private final UserModelRepository userModelRepository;

    @Autowired
    public UserModelDetailService(UserModelRepository userModelRepository) {
        this.userModelRepository = userModelRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final UserModel user = userModelRepository.findByEmailAndActive(email, true);

        if (user == null) {
            throw new UsernameNotFoundException("User with email " + email + " does not exists!");
        }

        final List<SimpleGrantedAuthority> authList = (List<SimpleGrantedAuthority>) user.getAuthorities();
        final String password = user.getPassword();

        return new User(email, password, authList);
    }
}
