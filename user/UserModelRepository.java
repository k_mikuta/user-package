package com.srpmed.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * User Model data access object
 */
@Repository
interface UserModelRepository extends CrudRepository<UserModel, Long> {

    UserModel findByEmail(String email);

    UserModel findByEmailAndActive(String email, boolean active);

    UserModel findByToken(String token);
}
