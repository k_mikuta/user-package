package com.srpmed.user;

import com.google.common.base.Preconditions;
import com.srpmed.exception.AssemblingProblemException;
import com.srpmed.profil_management.ProfileManagerDTO;
import com.srpmed.registration.RegistrationDTO;
import com.srpmed.util.dates.DateParser;

import java.text.ParseException;
import java.util.Date;

/**
 * Transform DTO objects to Model objects
 */
public class UserModelAssembler {

    /**
     * Transform form data object to model object, which can be stored in database
     *
     * @param registrationDTO form data object
     * @return UserModel object
     * @throws AssemblingProblemException if error occurred while assembling
     */
    public static UserModel fromRegistrationDTO(RegistrationDTO registrationDTO) {
        Preconditions.checkArgument(registrationDTO != null);

        UserModel userModel = new UserModel();
        userModel.setFirstName(registrationDTO.getFirstName());
        userModel.setLastName(registrationDTO.getLastName());
        userModel.setEmail(registrationDTO.getEmail());
        userModel.setPassword(registrationDTO.getPassword());
        userModel.setSex(registrationDTO.getSexCode());
        userModel.setBirthDate(dateFromString(registrationDTO.getBirthDate()));
        userModel.setPesel(registrationDTO.getPesel());
        userModel.setAddressModel(prepareUserAddressModel(registrationDTO));
        userModel.setRoles(UserRoleEnum.ROLE_USER.roleName());

        return userModel;
    }

    private static UserAddressModel prepareUserAddressModel(RegistrationDTO registrationDTO) {
        UserAddressModel userAddressModel = new UserAddressModel();
        userAddressModel.setStreetName(registrationDTO.getStreetName());
        userAddressModel.setHouseNumber(registrationDTO.getHouseNumber());
        userAddressModel.setFlatNumber(registrationDTO.getFlatNumber());
        userAddressModel.setPostcode(registrationDTO.getPostcode());
        userAddressModel.setCity(registrationDTO.getCity());
        userAddressModel.setProvince(registrationDTO.getProvince());

        return userAddressModel;
    }

    private static Date dateFromString(String dateStr) {
        try {
            return DateParser.fromString(dateStr);
        } catch (ParseException e) {
            throw new AssemblingProblemException(e.getMessage());
        }
    }
}